.. _pocketbeagle-home:

PocketBeagle
###################

.. admonition:: Contributors

   This work is licensed under a `Creative Commons Attribution-ShareAlike
   4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__
   
   - Maintaining author: `Jason Kridner <jkridner@beagleboard.org>`_
   - Contributing Editor: `Cathy Wicks  <cathy@beagleboard.org>`_

.. note::
    Make sure to read and accept all the terms & condition provided in the :ref:`boards-terms-and-conditions` page. 
    
    Use of either the boards or the design materials constitutes agreement to the T&C including any 
    modifications done to the hardware or software solutions provided by beagleboard.org foundation.

PocketBeagle is an ultra-tiny-yet-complete open-source USB-key-fob computer. 
PocketBeagle features an incredible low cost, slick design and simple usage, 
making PocketBeagle the ideal development board for beginners and professionals alike. 

.. image:: images/PocketBeagle-size-compare-small.jpg
   :width: 598
   :align: center
   :height: 400
   :alt: PocketBeagle

.. toctree::
   :maxdepth: 1

   /boards/pocketbeagle/original/ch01.rst
   /boards/pocketbeagle/original/ch02.rst
   /boards/pocketbeagle/original/ch03.rst
   /boards/pocketbeagle/original/ch04.rst
   /boards/pocketbeagle/original/ch05.rst
   /boards/pocketbeagle/original/ch06.rst
   /boards/pocketbeagle/original/ch07.rst
   /boards/pocketbeagle/original/ch08.rst
   /boards/pocketbeagle/original/ch09.rst
   /boards/pocketbeagle/original/ch10.rst
   /boards/pocketbeagle/original/ch11.rst
