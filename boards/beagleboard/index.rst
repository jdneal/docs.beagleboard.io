.. _beagleboard-boards-home:

BeagleBoard (all)
###################

BeagleBoard boards are low-cost, ARM-based development boards suitable for rapid prototyping and 
open-hardware to enable professionals to develop production systems.

.. note::
    Make sure to read and accept all the terms & condition provided in the :ref:`boards-terms-and-conditions` page. 
    
    Use of either the boards or the design materials constitutes agreement to the T&C including any 
    modifications done to the hardware or software solutions provided by beagleboard.org foundation.

The latest PDF-formatted System Reference Manual for each BeagleBoard board is linked below.

* `BeagleBoard <https://git.beagleboard.org/beagleboard/beagleboard/-/blob/master/BeagleBoard_revC5_SRM.pdf>`_
* `BeagleBoard-xM <https://git.beagleboard.org/beagleboard/beagleboard-xm/-/blob/master/BeagleBoard-xM_SRM.pdf>`_
* `BeagleBoard-X15 <https://git.beagleboard.org/beagleboard/beagleboard-x15/-/blob/master/BeagleBoard-X15_SRM.pdf>`_

